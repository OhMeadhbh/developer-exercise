class Exercise

  # Assume that "str" is a sequence of words, which are sequences of alphabetic characters.
  # Return a string in which every word in "str" that exceeds 4 characters is replaced with "marklar".
  # If the word being replaced has a capital first letter, it should instead be replaced with "Marklar".
  def self.marklar(str)

    marklarize = lambda {
      |x|
      if x.length > 4
        if x.capitalize == x
          x.gsub( /[[:alpha:]]+/, "Marklar" )
        else
          x.gsub( /[[:alpha:]]+/, "marklar" )
        end
      else
        x
      end
    }
    
    str.split.map(&marklarize).join( " " );
  end

  # Return the sum of all even numbers in the Fibonacci sequence, up to
  # the "nth" term in the sequence
  # eg. the Fibonacci sequence up to 6 terms is (1, 1, 2, 3, 5, 8),
  # and the sum of its even numbers is (2 + 8) = 10
  def self.even_fibonacci(nth)
    prev0 = 1
    prev1 = 1
    m = 0
    e = 0
    (3..nth).each{
      |i|
      m = prev0 + prev1
      prev1 = prev0
      prev0 = m
      if ( m % 2 ) == 0
        e = e + m
      end
    }
    e
  end

end

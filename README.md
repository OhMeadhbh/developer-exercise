## Jaguar Design Studio
## Web Application Developer Exercise

### Introduction

The following exercises are to be completed by web developer candidates applying for a position at Jaguar Design Studio. You will need to use Git and a Bitbucket account to complete and submit the exercises.

It is expected that some applicants will have no previous experience with Git and/or Bitbucket. Successfully using these tools is part of the exercise.

Follow the instructions that were provided in the job posting for details on how to complete this exercise.

### Exercise: Ruby

Candidates are not necessarily expected to be familiar with the Ruby language, but the following exercises are intended to be simple enough that a qualified programmer should be able to complete them after spending a couple of hours learning basic Ruby syntax, and a little consulting of the Ruby language docs.

To complete this exercise, implement the incomplete methods defined in "exercise.rb", such that they pass the tests defined in "exercise_spec.rb".

In order to run the tests, you will need Minitest installed. If you are running ruby v1.9 or greater, Minitest is already installed.

To execute the test suite, run:

    $ ruby exercise_spec.rb

If you are running a version of ruby less than 1.9, install the Minitest package first:

    $ sudo gem install minitest
    $ ruby exercise_spec.rb

If this fails with a LoadError message indicating the 'require' call has failed, try explicitly specifying the directory where the gem was installed:

    $ ruby -I/var/lib/gems/1.8/gems/minitest-5.0.8/lib exercise_spec.rb

The Exercise class is correctly implemented once the test suite returns:

    4 runs, 8 assertions, 0 failures, 0 errors, 0 skips
